#!/usr/bin/env python3
import os
import requests

url = "http://34.68.86.233/fruits/"
descriptions_path = "./supplier-data/descriptions"
images_extension = ".jpeg"
#line_title = ["name", "weight", "description", "image_name"]
line_title = ["name", "weight", "description"]
weight_line = 1
line_number = 0

def post_descriptions():
    descriptions_list = os.listdir(descriptions_path)
    for desc_file in descriptions_list:
        dict = {}
        dfile = descriptions_path + "/" + desc_file
        print('Description file: {}'.format(dfile))
        line_number = 0
        with open(descriptions_path + "/" + desc_file) as file:
            for line in file:
                if line_number == weight_line:
                  weight = line.split()
                  dict[line_title[line_number]] = int(weight[0])
                elif line_number <= 2:
                  print('line number {}'.format(line_number))
                  dict[line_title[line_number]] = line.strip()
                line_number += 1
            #print('line number {}'.format(line_number))
            #line_number -= 1
            # Add Image name
            f, e = os.path.splitext(desc_file)
            image_name = f + images_extension
            #print('Image name: {}'.format(image_name))
            dict["image_name"] = image_name
        print(dict)
        resp = requests.post(url, data=dict)

def main():
  post_descriptions()

if __name__ == "__main__":
  main()