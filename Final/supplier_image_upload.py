#!/usr/bin/env python3
import requests
import glob

url = "http://34.68.86.233/upload/"
images_path = "./supplier-data/images"
files_pattern = "*.jpeg"

def post_images():
  files_list = glob.glob(images_path + "/" + files_pattern)
  for image_file in files_list:
    print(image_file)
    with open(image_file, 'rb') as opened:
      r = requests.post(url, files={'file': opened})

def main():
  post_images()

if __name__ == "__main__":
  main()
