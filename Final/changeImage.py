#!/usr/bin/env python3
import os
from PIL import Image

images_path = "./supplier-data/images"

def change_image_size():
  images_list = os.listdir(images_path)
  for image_file in images_list:
    infile = images_path + "/" + image_file
    print(infile)
    f, e = os.path.splitext(image_file)
    outfile = images_path + "/" + f + ".jpeg"
    print(outfile)
    if infile != outfile and e != '':
      try:
        with Image.open(images_path + "/" + image_file) as im:
          new_im = im.convert("RGB").resize((600,400))
          new_im.save(outfile)
      except OSError:
        print("Cannot convert", image_file)

def main():
  change_image_size()

if __name__ == "__main__":
  main()
