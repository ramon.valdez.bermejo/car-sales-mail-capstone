#!/usr/bin/env python3
import emails
import os
import reports
from datetime import date

descriptions_path = "./supplier-data/descriptions"

def report_body():
  additional_info = ""
  descriptions_list = os.listdir(descriptions_path)
  for desc_file in descriptions_list:
    line_number = 0
    with open(descriptions_path + "/" + desc_file) as file:
      for line in file:
        if line_number == 0:
          additional_info = additional_info + "name: " + line + "<br/>
        elif line_number == 1:
          additional_info = additional_info + "weight: " + line + "<br/><br/>"
        else:
          break
        line_number += 1
  return additional_info


def main():
  today = date.today()

  title = "Processed update on " + today.strftime("%B %d, %Y") + "\n\n"
  reports.generate_report("/tmp/processed.pdf", title, report_body())
  sender = "automation@example.com"
  receiver = "{}@example.com".format(os.environ.get('USER'))
  subject = "Upload Completed - Online Fruit Store"
  body = "All fruits are uploaded to our website successfully. A detailed list is attached to this email."
  message = emails.generate_email(sender, receiver, subject, body, "/tmp/processed.pdf")
  emails.send_email(message)

if __name__ == "__main__":
  main()

