# Capston Project.
#!/usr/bin/env python3
import os
import requests

# Publish feedback

files_path = "/data/feedback"
line_title = ["title", "name", "date", "feedback"]
feedback_url = "http://34.71.188.34/feedback/"

def publish_feedbacks():
    feedback_list = os.listdir(files_path)
    for feedback_file in feedback_list:
       print(files_path + "/" + feedback_file)
       dict = {}
       with open(files_path + "/" + feedback_file) as file:
          line_number = 0
          for line in file:
             dict[line_title[line_number]] = line
             line_number += 1
       print(dict)
       resp = requests.post(feedback_url, data=dict)
       print(resp.text)
       print(resp.status_code)
       if (resp.status_code == 201):
          print("Successfuly published!")

if __name__ == '__main__':
    retrieve_feedbacks()
